[![pipeline status](https://gitlab.com/kelompok-2-ppw-a/Kelompok-2-PPW-A/badges/master/pipeline.svg)](https://gitlab.com/kelompok-2-ppw-a/Kelompok-2-PPW-A/commits/master)

[![coverage report](https://gitlab.com/kelompok-2-ppw-a/Kelompok-2-PPW-A/badges/master/coverage.svg)](https://gitlab.com/kelompok-2-ppw-a/Kelompok-2-PPW-A/commits/master)


Nama Anggota Kelompok:

1. Azahra Putri Andani
2. Jessica Alexia Jaury
3. M. Ammar Ramadhan
4. Stefan Mayer S.

Link Heroku:
[kelompok-2-ppw-a.herokuapp.com](https://kelompok-2-ppw-a.herokuapp.com)

[https://gitlab.com/kelompok-2-ppw-a/Kelompok-2-PPW-A.git](https://gitlab.com/kelompok-2-ppw-a/Kelompok-2-PPW-A.git)
