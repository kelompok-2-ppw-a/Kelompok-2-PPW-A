from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

# Create your tests here.
class DashboardUnitTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response = Client().get('/stats/')
        self.assertEqual(response.status_code,200)

    def test_dashboard_using_index_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func,index)