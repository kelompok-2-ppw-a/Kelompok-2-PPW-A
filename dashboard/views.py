from django.shortcuts import render
from tugas_addfriend.models import Friend
from tugas_updateStatus.models import Status
from tugas_profile.views import url_pp, name

# Create your views here.
friend = Friend.objects.all().count()
feed = Status.objects.all().count()
data = Status.objects.filter(id=feed-1).values()
# Create your views here.

def index(request):
    response = {'name':name,'friend':friend, 'feed':feed, 'pp':url_pp,'status':data}
    return render(request,'dashboard/tugas_profile.html',response)