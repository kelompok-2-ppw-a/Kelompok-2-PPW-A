from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Profile
from datetime import datetime
import pytz
import json

# Create your views here.
response = {}
def index(request):    
	response['author'] = "Moh Ammar R" #TODO Implement yourname
    html = 'lab_5/lab_5.html'
	return render(request, html, response)

def edit(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['description'] = request.POST['description']
        response['gender'] = request.POST['gender']
        response['email'] = request.POST['email']
        response['expertise'] = request.POST['expertise']
        response['pp'] = request.POST['pp']
        try:
            date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
        except Exception:
            return HttpResponseRedirect('/profile/')
        
        profile = Profile(name=response['name'],description=response['description'],birthdate=date.replace(tzinfo=pytz.UTC),gender=response['gender'],email=response['email'],expertise=response['expertise'],pp=response['pp'])
        profile.save()
        return HttpResponseRedirect('/profile/')
    else:
        return HttpResponseRedirect('/profile/')