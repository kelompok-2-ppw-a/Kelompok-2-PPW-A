from django.db import models
from multiselectfield import MultiSelectField

class Profile(models.Model):
	EXPRESTISE = (
		(1,'Marketing'),
		(2,'Collector'),
		(3,'Public Speaking'),
		(4,'Programmer'),
		(5,'Designer'),
		(6,'Architect'),
		)

	GENDER = (
		(1,'Male'),
		(2,'Female'),
		)

	name = models.CharField(max_length=50)
	description = models.TextField()
	birthdate = models.DateField()
	gender = MultiSelectField(choices=GENDER,
                                 max_choices=2,
                                 max_length=2)
	email = models.TextField()
	expertise = MultiSelectField(choices=EXPERTISE,
                                 max_choices=6,
                                 max_length=6)
	pp = models.TextField()
