from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend

import datetime

# Create your views here.
response = {}

def index(request):
	response['friend_form'] = Friend_Form
	friends = Friend.objects.all()
	response['friend'] = friends
	html = 'tugas_addfriend/tugas_addfriend.html'
	return render(request, html, response)

def message_post(request):
	form = Friend_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name']
		response['url'] = request.POST['url']
		friend = Friend(name=response['name'], url=response['url'])
		friend.save()
		return HttpResponseRedirect('/friends/')
	else:
		return HttpResponseRedirect('/friends/')