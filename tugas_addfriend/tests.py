from django.test import TestCase, Client
from .models import Friend
from .forms import Friend_Form

# Create your tests here.

class AddFriendUnitTest(TestCase):

	def test_add_friend_url_is_exist(self):
		response = Client().get('/friends/')
		self.assertEqual(response.status_code, 200)

	def test_model_can_add_friend(self):
		new_friend = Friend.objects.create(name='Azahra', url='https://zahra-first-try.herokuapp.com')
		counting_all_available_friend = Friend.objects.all().count()
		self.assertEqual(counting_all_available_friend,1)

	def test_form_validation_for_blank_items(self):
		form = Friend_Form(data={'name':'', 'url':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['name'], ["This field is required."])
		self.assertEqual(form.errors['url'], ["This field is required."])

	def test_add_friend_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/friends/add_friend', {'name': '', 'url': ''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/friends/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	def test_showing_all_friends(self):
		name_one = 'One'
		url_one = 'https://one.herokuapp.com'
		data_one = {'name':name_one, 'url':url_one}
		add_friend_one = Client().post('/friends/add_friend', data_one)
		self.assertEqual(add_friend_one.status_code, 302)

		name_two = 'Two'
		url_two = 'https://two.herokuapp.com'
		data_two = {'name':name_two, 'url':url_two}
		add_friend_two = Client().post('/friends/add_friend', data_two)
		self.assertEqual(add_friend_two.status_code, 302)

		response = Client().get('/friends/')
		html_response = response.content.decode('utf8')

		for key,data in data_one.items():
			self.assertIn(data,html_response)

	def test_add_friend_has_placeholder_and_css_classes(self):
		form = Friend_Form()
		self.assertIn('class="form-control-made"', form.as_p())
		self.assertIn('<label for="id_name">Name:</label>', form.as_p())
		self.assertIn('<label for="id_url">URL:</label>', form.as_p())

	# def test_add_success_and_render_the_result(self):
	# 	name = 'Zahra'
	# 	url = 'https://zahra.herokuapp.com'
	# 	data_zahra = {'name':name, 'url':url}
	# 	add_friend_zahra = Client().post('/tugas-addfriend/add_friend', data_zahra)
	# 	added = Client().get('/tugas-addfriend/add_friend', data_zahra)
	# 	self.assertEqual(add_friend_zahra.status_code, 302)
	# 	html_response = add_friend_zahra.content.decode('utf8')
	# 	self.assertIn(name, html_response)
	# 	# self.assertIn(url, html_response)