# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-08 16:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tugas_addfriend', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='friend',
            old_name='created_date',
            new_name='date',
        ),
    ]
