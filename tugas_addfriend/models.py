from django.db import models
import datetime
#import pytz
#from timezone_field import TimeZoneField

# Create your models here.

class Friend(models.Model):
	name = models.CharField(max_length=100)
	url = models.URLField()
	date = models.DateTimeField(auto_now_add=True)


