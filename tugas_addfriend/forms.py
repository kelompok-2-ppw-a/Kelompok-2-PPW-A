from django import forms

class Friend_Form(forms.Form):
	error_messages = {'required' : 'Please fill in the field',
						'invalid' : 'Please fill in the field with the valid URL'}

	attrs = {
    'class': 'form-control-made'
    }

	name = forms.CharField(max_length=100, label='Name', required=True, widget=forms.TextInput(attrs=attrs))
	url = forms.URLField(label='URL', required=True, min_length=14, max_length=100, initial = "https://", 
			widget=forms.URLInput(attrs=attrs))