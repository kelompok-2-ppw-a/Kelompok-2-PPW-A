from django.shortcuts import render

#TODO Implement
#Create a content paragraph for your landing page:
name = "Sawada Tsunayoshi"
expertise = ["Marketing","Collector","Public Speaking"]
bio_dict = [
{'subject' : 'Birthday', 'value' : '19 Agustus 1970'},\
{'subject' : 'Gender', 'value' : 'Female'},\
{'subject' : 'Expertise', 'value' : expertise},\
{'subject' : 'Description', 'value' : 'Antique expert, Experience as marketer for 10 years'},\
{'subject' : 'Email', 'value' : 'hello@smith.com'},\
]
url_pp = "https://images.8tracks.com/cover/i/009/677/418/Sawada.Tsunayoshi.full.1023723-7467.jpg?"
def index(request):
    response = {'name': name, 'bio_dict': bio_dict, 'pp':url_pp}
    return render(request, 'tugas_profile/tugas_profile.html', response)
