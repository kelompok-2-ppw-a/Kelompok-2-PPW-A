from django.apps import AppConfig


class TugasProfileConfig(AppConfig):
    name = 'tugas_profile'
