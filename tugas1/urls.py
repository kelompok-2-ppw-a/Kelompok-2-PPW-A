"""tugas1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import tugas_profile.urls as tugas_profile
import tugas_addfriend.urls as tugas_addfriend
import tugas_updateStatus.urls as tugas_updateStatus
import dashboard.urls as dashboard
from django.views.generic import RedirectView

urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^profile/', include(tugas_profile,namespace='tugas_profile')),
	url(r'^friends/', include(tugas_addfriend, namespace='tugas_addfriend')),
	url(r'^stats/', include(dashboard, namespace='dashboard')),
    url(r'^updateStatus/', include(tugas_updateStatus, namespace='tugas_updateStatus')),
    ]