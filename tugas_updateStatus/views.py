from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}
def index(request):    
    response['author'] = "Kelompok 2" #TODO Implement yourname
    status = Status.objects.all()
    response['status'] = status
    html = 'tugas_updateStatus/tugas_updateStatus.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/updateStatus/')
    else:
        return HttpResponseRedirect('/updateStatus/')

