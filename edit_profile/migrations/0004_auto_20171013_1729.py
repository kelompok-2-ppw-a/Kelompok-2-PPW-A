# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-13 10:29
from __future__ import unicode_literals

from django.db import migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('edit_profile', '0003_auto_20171013_1724'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='Expertise',
            field=multiselectfield.db.fields.MultiSelectField(choices=[('Marketing', 'Marketing'), ('Collector', 'Collector'), ('Public Speaking', 'Public Speaking'), ('Programmer', 'Programmer'), ('Designer', 'Designer'), ('Architect', 'Architect')], max_length=65),
        ),
        migrations.AlterField(
            model_name='profile',
            name='Gender',
            field=multiselectfield.db.fields.MultiSelectField(choices=[('Male', 'Male'), ('Female', 'Female')], max_length=11),
        ),
    ]
