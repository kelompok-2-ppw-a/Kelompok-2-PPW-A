from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Profile
from .forms import Profile_Form
from datetime import datetime
import pytz
import json

# Create your views here.
response = {}
def index(request):
    response['author'] = "Moh Ammar R" #TODO Implement yourname
    response['form'] = Profile_Form
    html = 'profile/edit_profile.html'
    return render(request, html, response)

def edit(request):
    form = Profile_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        expertise = form.cleaned_data.get('Expertise')
        response['Name'] = request.POST['name']
        response['Description'] = request.POST['description']
        response['Expertise'] = expertise
        response['Gender'] = request.POST['Gender']
        response['Email'] = request.POST['email']
        response['PP'] = request.POST['pp']
        try:
            date = datetime.strptime(request.POST['date'],'%d-%m-%Y')
        except Exception:
            return HttpResponseRedirect('/profile/')
        if(Profile.objects.all().count()==0):
            profile = Profile(Name=response['Name'],Description=response['Description'],Birthdate=date.replace(tzinfo=pytz.UTC),Gender=response['Gender'],Email=response['Email'],Expertise=response['Expertise'],PP=response['PP'])
            profile.save()
        else:
            Profile.objects.filter(id=1).update(Name=response['Name'],Description=response['Description'],Birthdate=date.replace(tzinfo=pytz.UTC),Gender=response['Gender'],Email=response['Email'],Expertise=response['Expertise'],PP=response['PP'])
        return HttpResponseRedirect('/profile/')
    else:
        return HttpResponseRedirect('/edit_profile/')