from django import forms
from 
class Profile_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    EXPERTISE = (
        ('Marketing','Marketing'),
        ('Collector','Collector'),
        ('Public Speaking','Public Speaking'),
        ('Programmer','Programmer'),
        ('Designer','Designer'),
        ('Architect','Architect'),
        )

    GENDER = (
        (1,'Male'),
        (2,'Female'),
        )

    Gender = forms.MultipleChoiceField(widget=forms.RadioSelect(),
                                             choices=GENDER)
    Expertise = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                                             choices=EXPERTISE)
    