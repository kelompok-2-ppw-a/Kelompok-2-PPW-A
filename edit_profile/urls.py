from django.conf.urls import url
from .views import index, edit

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^confirm-edit', edit, name='confirm-edit'),
]
		