from django.db import models
from multiselectfield import MultiSelectField
class Profile(models.Model):
	EXPERTISE = (
		('Marketing','Marketing'),
		('Collector','Collector'),
		('Public Speaking','Public Speaking'),
		('Programmer','Programmer'),
		('Designer','Designer'),
		('Architect','Architect'),
		)

	GENDER = (
		(1,'Male'),
		(2,'Female'),
		)

	Name = models.CharField(max_length=50)
	Description = models.TextField()
	Birthdate = models.DateField()
	Gender = MultiSelectField(choices=GENDER)
	Email = models.EmailField()
	Expertise = MultiSelectField(choices=EXPERTISE)
	PP = models.TextField()
